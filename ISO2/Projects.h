//
//  Projects.h
//  ISO2
//
//  Created by IT  on 20/7/16.
//  Copyright © 2016 Richart. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Projects : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Projects+CoreDataProperties.h"
