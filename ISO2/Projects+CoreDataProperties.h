//
//  Projects+CoreDataProperties.h
//  ISO2
//
//  Created by IT  on 21/7/16.
//  Copyright © 2016 Richart. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Projects.h"

NS_ASSUME_NONNULL_BEGIN

@interface Projects (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *customer;
@property (nullable, nonatomic, retain) NSString *details;
@property (nullable, nonatomic, retain) NSData *image;
@property (nullable, nonatomic, retain) NSString *name;

@end

NS_ASSUME_NONNULL_END
