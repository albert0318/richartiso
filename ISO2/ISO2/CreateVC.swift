//
//  CreateVC.swift
//  ISO2
//
//  Created by IT  on 20/7/16.
//  Copyright © 2016 Richart. All rights reserved.
//
// ZuQ8LpwcPl!V

import UIKit
import CoreData

class CreateVC: UIViewController,NSFetchedResultsControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate, UITextFieldDelegate {
    
    var item : Projects? = nil
    

    @IBOutlet weak var customerName: UITextField!
    @IBOutlet weak var projectName: UITextField!
    @IBOutlet weak var details: UITextField!
    @IBOutlet weak var imageHolder: UIImageView!
    @IBOutlet weak var projectPicker: UIPickerView!
    @IBOutlet weak var customerPicker: UIPickerView!
    @IBOutlet weak var durato: UIDatePicker!
    @IBOutlet weak var durafrom: UIDatePicker!
    @IBOutlet weak var discudate: UIDatePicker!
    @IBOutlet weak var setupdate: UIDatePicker!
    
    
    let projects = ["Christmas", "CNY", "Halloween", "Easter", "Others"]
    let customers = ["Comapny A", "Comapny B", "Comapny C", "Comapny D", "Comapny E", "Comapny F", "Comapny G", "Comapny H", "Comapny I", "Comapny J"]
    let moc = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        projectPicker.dataSource = self
        projectPicker.delegate = self
        customerPicker.dataSource = self
        customerPicker.delegate = self
        
        if item != nil {
            projectName.text = item?.name
            customerName.text = item?.customer
            details.text = item?.details
            imageHolder.image = UIImage(data: (item?.image)!)
        }

        // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func dismissVC() {
        
        navigationController?.popViewControllerAnimated(true)
        
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
        self.imageHolder.image = image
    }
    
    
    func createNewItem() {
        
        let entityDescription = NSEntityDescription.entityForName("Projects", inManagedObjectContext: moc)
        
        let item = Projects(entity: entityDescription!, insertIntoManagedObjectContext: moc)
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy"
        item.name = customerName.text! + " " + projectName.text! + " " + dateFormatter.stringFromDate((setupdate.date))
        item.customer = customerName.text
        item.details = details.text
        item.datedurto = durato.date
        item.datedurfrom = durafrom.date
        item.datesetup = setupdate.date
        item.datediscussed = discudate.date
        item.image = UIImagePNGRepresentation(imageHolder.image!)
        
        do {
            try moc.save()
        } catch {
            return
        }
        
    }
    
    func editItem() {
        
        item?.name = projectName.text
        item?.customer = customerName.text
        item?.details = details.text
        item!.image = UIImagePNGRepresentation(imageHolder.image!)
        
        do {
            try moc.save()
        } catch {
            return
        }
    }
    
    @IBAction func saveAdd(sender: AnyObject) {
        
        if item != nil {
            editItem()
        } else {
            createNewItem()
        }
        
        dismissVC()
    }
  
    @IBAction func cancelAdd(sender: AnyObject) {
        
         dismissVC()
        
    }

    @IBAction func cameraImage(sender: AnyObject) {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.sourceType = UIImagePickerControllerSourceType.Camera
        pickerController.allowsEditing = false
        self.presentViewController(pickerController, animated: true, completion: nil)
        
    }
    @IBAction func deviceImage(sender: AnyObject) {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        pickerController.allowsEditing = false
        
        self.presentViewController(pickerController, animated: true, completion: nil)
    }
    
    
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
        return projects.count
        }else{
        return customers.count
        }
        
        
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1{
        return projects[row]
        }else{
        return customers[row]
        }
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1 {
            projectName.text = projects[row]
            if row == 4{
                projectName.text = ""
                projectName.hidden = false
            }else{
                projectName.text = projects[row]
                projectName.hidden = true
            }
                
        }else{
            customerName.text = customers[row]
        }
}
        




    
 
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

