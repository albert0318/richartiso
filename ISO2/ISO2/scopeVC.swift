//
//  scopeVC.swift
//  ISO2
//
//  Created by IT  on 29/7/16.
//  Copyright © 2016 Richart. All rights reserved.
//

import UIKit
import CoreData

class scopeVC: UIViewController, NSFetchedResultsControllerDelegate {

    var mydata = selectedData()
    var item : Projects? = nil
    
    @IBOutlet weak var submitby: UILabel!
    @IBOutlet weak var details: UILabel!
    @IBOutlet weak var dismantledate: UILabel!
    @IBOutlet weak var setupdate: UILabel!
    @IBOutlet weak var jobno: UILabel!
    @IBOutlet weak var regno: UILabel!
    @IBOutlet weak var projectName: UILabel!
    @IBOutlet weak var customer: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tbc = tabBarController as! projTab
        mydata = tbc.datapass
        item = mydata.dataPass()
        if item != nil{
            customer.text = item?.customer
            projectName.text = item?.name
            details.text = item?.details
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            setupdate.text = dateFormatter.stringFromDate((item?.datesetup)!)
            dismantledate.text = dateFormatter.stringFromDate((item?.datediscussed)!)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
