//
//  projectCollectionVC.swift
//  ISO2
//
//  Created by IT  on 21/7/16.
//  Copyright © 2016 Richart. All rights reserved.
//

import UIKit
import CoreData

private let reuseIdentifier = "Cell"

class projectCollectionVC: UICollectionViewController, NSFetchedResultsControllerDelegate  {
    
    var mytitle = ref()
    var bac: String = ""
    var c:Int = 0
    var mydata = selectedData()
    var item : Projects? = nil

    
    @IBOutlet weak var projectName: UINavigationItem!
    
    
    let moc = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    var frc : NSFetchedResultsController = NSFetchedResultsController()

    
    func fetchRequest() -> NSFetchRequest {
        
        let fetchRequest = NSFetchRequest(entityName: "Visuals")
        let sortDescriptor = NSSortDescriptor(key: "project", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.predicate = NSPredicate(format: "project == %@", (mytitle.titlePass()))
        return fetchRequest
        
    }
    
    func getFRC() -> NSFetchedResultsController {
        
        frc = NSFetchedResultsController(fetchRequest: fetchRequest(), managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
        return frc
    }

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let tbc = tabBarController as! projTab
        mydata = tbc.datapass
        item = mydata.dataPass()
        if c == 0 {
        let tbc = tabBarController as! projTab
        mytitle = tbc.mytitle
            projectName.title = mytitle.titlePass()
        }
        frc = getFRC()
        frc.delegate = self
        
        do {
            try frc.performFetch()
        } catch {
            print("Failed to perform initial fetch.")
            return
        }
        
        if bac != ""{
            projectName.title = bac
        }
        self.collectionView?.reloadData()

        

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        frc = getFRC()
        frc.delegate = self
        
        do {
            try frc.performFetch()
        } catch {
            print("Failed to perform initial fetch.")
            return
        }
        
        self.collectionView?.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

//    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 0
//    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let numberOfRowsInSection = frc.sections?[section].numberOfObjects
        
        return numberOfRowsInSection!
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("colcell", forIndexPath: indexPath) as UICollectionViewCell
    
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = UIColor.clearColor()
        } else {
            cell.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
        }
        
    
        let item = frc.objectAtIndexPath(indexPath) as! Visuals
        
        let image = cell.viewWithTag(2) as! UIImageView
        
       image.image = UIImage(data: (item.photo)!)
 //       NSDateFormatter().stringFromDate(item.date!)
        // Configure the cell
    
        return cell
    }



    @IBAction func done(sender: AnyObject) {
        
        let tabc = tabBarController as! projTab
        tabc.dismissVC()
    }

    @IBAction func transition(sender: UIBarButtonItem) {
        let secondViewController:addVisualVCViewController = addVisualVCViewController()
        
        self.presentViewController(secondViewController, animated: true, completion: nil)
        
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        let segueID = segue.identifier ?? ""
        
        switch segueID
        {
        case "vi":
            let nc = segue.destinationViewController as! UINavigationController
            let destinationVC = nc.topViewController as! addVisualVCViewController
            destinationVC.pass = projectName.title!
            destinationVC.item = item
        
        default:
            break
        }
    }



}
