//
//  addVisualVCViewController.swift
//  ISO2
//
//  Created by IT  on 21/7/16.
//  Copyright © 2016 Richart. All rights reserved.
//

import UIKit
import CoreData

class addVisualVCViewController: UIViewController,NSFetchedResultsControllerDelegate , UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var pass:String = ""
    var mydata = selectedData()
    var item : Projects? = nil

    
    @IBOutlet weak var projectName: UILabel!
    @IBOutlet weak var imageHolder: UIImageView!
    @IBOutlet weak var namesss: UINavigationItem!
    
     let moc = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        projectName.text = pass
        namesss.title = pass

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func dismissVC() {
        
       let vnc = UINavigationController() as! visualViewController
        vnc.dismissVC()
        
    }
    
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
        self.imageHolder.image = image
    }

    
    func createNewItem() {
        
        let entityDescription = NSEntityDescription.entityForName("Visuals", inManagedObjectContext: moc)
        
        let item = Visuals(entity: entityDescription!, insertIntoManagedObjectContext: moc)
        
        item.project = namesss.title
        item.date = NSDate()
        item.photo = UIImagePNGRepresentation(imageHolder.image!)
        
        do {
            try moc.save()
        } catch {
            return
        }
        
    }
    func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool) {
        if navigationController.isKindOfClass(UIImagePickerController.self) {
            viewController.navigationController!.navigationBar.translucent = false
            viewController.edgesForExtendedLayout = .None
        }
    }
    
    @IBAction func deviceUpload(sender: AnyObject) {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        pickerController.allowsEditing = false
        self.presentViewController(pickerController, animated: true, completion: nil)
    }

    @IBAction func cameraUpload(sender: AnyObject) {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.sourceType = UIImagePickerControllerSourceType.Camera
        pickerController.allowsEditing = false
        
        self.presentViewController(pickerController, animated: true, completion: nil)
    }
    @IBAction func addVisual(sender: AnyObject) {
        createNewItem()
    }
    @IBAction func cancelVisual(sender: AnyObject) {
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        let segueID = segue.identifier ?? ""
        
        switch segueID
        {
        case "bac":
            createNewItem()
            let itemController : projTab = segue.destinationViewController as! projTab
            itemController.item = item
            itemController.num = 2
            
        case "bac2":
            let itemController : projTab = segue.destinationViewController as! projTab
            itemController.item = item
            itemController.num = 2

        default:
            break
        }
    }

}
