//
//  ProdWorkOrderVC.swift
//  ISO2
//
//  Created by IT  on 2/8/16.
//  Copyright © 2016 Richart. All rights reserved.
//

import UIKit

class ProdWorkOrderVC: UIViewController {

    var items : Projects? = nil
    var item : WorkOrder? = nil
    
    
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var divi: UILabel!
    @IBOutlet weak var customer: UILabel!
    @IBOutlet weak var proj: UILabel!
    @IBOutlet weak var comple: UILabel!
    @IBOutlet weak var hando: UILabel!
    @IBOutlet weak var inpro: UILabel!
    @IBOutlet weak var final: UILabel!
    @IBOutlet weak var details: UITextView!
    @IBOutlet weak var imageHolder: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        date.text = dateFormatter.stringFromDate((item?.dateisue)!)
        divi.text = item?.division
        customer.text = items?.customer
        proj.text = items?.name
        comple.text = dateFormatter.stringFromDate((item?.dateisue)!)
        hando.text = dateFormatter.stringFromDate((item?.dateisue)!)
        inpro.text = dateFormatter.stringFromDate((item?.dateisue)!)
        final.text = dateFormatter.stringFromDate((item?.dateisue)!)
        details.text = item?.details
        imageHolder.image = UIImage(data: (item!.image)!)
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func done(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
