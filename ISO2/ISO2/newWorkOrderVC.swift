//
//  newWorkOrderVC.swift
//  ISO2
//
//  Created by IT  on 1/8/16.
//  Copyright © 2016 Richart. All rights reserved.
//

import UIKit
import CoreData

class newWorkOrderVC: UIViewController,NSFetchedResultsControllerDelegate, UIImagePickerControllerDelegate, UIPickerViewDataSource,UIPickerViewDelegate, UITextFieldDelegate, UINavigationControllerDelegate {

    
    var items : Projects? = nil
    var divs:String = "Metal Works"
    
    @IBOutlet weak var divi: UIPickerView!
    @IBOutlet weak var hando: UIDatePicker!
    @IBOutlet weak var process: UIDatePicker!
    @IBOutlet weak var imageHolder: UIImageView!
    @IBOutlet weak var details: UITextView!
    @IBOutlet weak var final: UIDatePicker!
    @IBOutlet weak var comple: UIDatePicker!
    @IBOutlet weak var test: UILabel!
    
    
    let projects = ["Metal Works", "Fine Arts", "Electrical", "General", "Design"]
    let moc = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        divi.delegate = self
        divi.dataSource = self
        test.text = items?.name

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissVC() {
        
        navigationController?.popViewControllerAnimated(true)
        
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
        self.imageHolder.image = image
    }
    
    
    func createNewItem() {
        
        let entityDescription = NSEntityDescription.entityForName("WorkOrder", inManagedObjectContext: moc)
        
        let item = WorkOrder(entity: entityDescription!, insertIntoManagedObjectContext: moc)
        
        item.project = items?.name
        item.division = divs
        item.dateisue = NSDate()
        item.completion = comple.date
        item.final = final.date
        item.inprocess = process.date
        item.handover = hando.date
        item.details = details.text
        item.image = UIImagePNGRepresentation(imageHolder.image!)
        
        do {
            try moc.save()
        } catch {
            return
        }
        
    }

    
    @IBAction func saveEntry(sender: AnyObject) {
        createNewItem()
        dismissVC()
    }

    
    @IBAction func cancelEntry(sender: AnyObject) {
        dismissVC()
    }
    
    
    @IBAction func deviceupload(sender: AnyObject) {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        pickerController.allowsEditing = false
        
        self.presentViewController(pickerController, animated: true, completion: nil)

    }
    
    
    @IBAction func cameraupload(sender: AnyObject) {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.sourceType = UIImagePickerControllerSourceType.Camera
        pickerController.allowsEditing = false
        self.presentViewController(pickerController, animated: true, completion: nil)

    }

    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

            return projects.count
        
        
        
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
 
            return projects[row]


    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

            divs = projects[row]

    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        let segueID = segue.identifier ?? ""
        
        switch segueID
        {
        case "sav":
            createNewItem()
            let itemController : projTab = segue.destinationViewController as! projTab
            itemController.item = items
            itemController.num = 3
            
        case "can":
            let itemController : projTab = segue.destinationViewController as! projTab
            itemController.item = items
            itemController.num = 3
            
        default:
            break
        }
    }


}
