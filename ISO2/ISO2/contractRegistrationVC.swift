//
//  contractRegistrationVC.swift
//  ISO2
//
//  Created by IT  on 27/7/16.
//  Copyright © 2016 Richart. All rights reserved.
//

import UIKit
import CoreData

class contractRegistrationVC: UIViewController, NSFetchedResultsControllerDelegate {
    
    var mydata = selectedData()
    var item : Projects? = nil

    @IBOutlet weak var regNo: UILabel!
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var projectName: UILabel!
    @IBOutlet weak var durfrom: UILabel!
    @IBOutlet weak var durto: UILabel!
    @IBOutlet weak var setupdate: UILabel!
    @IBOutlet weak var dismantledate: UILabel!
    @IBOutlet weak var projdetails: UILabel!
    @IBOutlet weak var salesperson: UILabel!
    @IBOutlet weak var datenow: UILabel!
    
    
    let moc = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tbc = tabBarController as! projTab
        mydata = tbc.datapass
        item = mydata.dataPass()
        if item != nil{
    companyName.text = item?.customer
            projectName.text = item?.name
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            durto.text = dateFormatter.stringFromDate((item?.datedurto!)!)
            durfrom.text = dateFormatter.stringFromDate((item?.datedurfrom!)!)
            setupdate.text = dateFormatter.stringFromDate((item?.datesetup)!)
            dismantledate.text = dateFormatter.stringFromDate((item?.datediscussed!)!)
            datenow.text = dateFormatter.stringFromDate((NSDate()))
            projdetails.text = item?.details
            
        }


        // Do any additional setup after loading the view.
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
