//
//  Projects+CoreDataProperties.swift
//  ISO2
//
//  Created by IT  on 26/7/16.
//  Copyright © 2016 Richart. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Projects {

    @NSManaged var customer: String?
    @NSManaged var details: String?
    @NSManaged var image: NSData?
    @NSManaged var name: String?
    @NSManaged var datedurfrom: NSDate?
    @NSManaged var datedurto: NSDate?
    @NSManaged var datesetup: NSDate?
    @NSManaged var datediscussed: NSDate?
    @NSManaged var salesperson: String?

}
