//
//  WorkOrder+CoreDataProperties.swift
//  ISO2
//
//  Created by IT  on 1/8/16.
//  Copyright © 2016 Richart. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension WorkOrder {

    @NSManaged var completion: NSDate?
    @NSManaged var details: String?
    @NSManaged var division: String?
    @NSManaged var final: NSDate?
    @NSManaged var handover: NSDate?
    @NSManaged var image: NSData?
    @NSManaged var incharge: String?
    @NSManaged var inprocess: NSDate?
    @NSManaged var project: String?
    @NSManaged var dateisue: NSDate?

}
