//
//  Visuals+CoreDataProperties.swift
//  ISO2
//
//  Created by IT  on 21/7/16.
//  Copyright © 2016 Richart. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Visuals {

    @NSManaged var project: String?
    @NSManaged var date: NSDate?
    @NSManaged var photo: NSData?

}
