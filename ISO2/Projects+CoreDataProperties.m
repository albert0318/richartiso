//
//  Projects+CoreDataProperties.m
//  ISO2
//
//  Created by IT  on 21/7/16.
//  Copyright © 2016 Richart. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Projects+CoreDataProperties.h"

@implementation Projects (CoreDataProperties)

@dynamic customer;
@dynamic details;
@dynamic image;
@dynamic name;

@end
